# Projekt

Tematem projektu jest gra lokalna Chinczyk napisana w Python

  - Projek jest zaliczeniem przedmiotu Python w Polsko-Japońskiej Szkole Technik Komputerowtch
  - Projek jest pisany za pomocą biblioteki PyGame.
## Technologie

Technologie użyte do stworzenia projektu

 - [Python](https://pl.python.org/) w wersji 3.8.2
 - [PyGame](https://www.pygame.org/news) w wersji 1.9.6

## Instalacja

Pobrać projekt z GitLab. Za pomocą PyCharm odpalić program. 

## Licencja

Projek Wykonany przez Studentów PJATK w celach edukacjnych

- Patryk Szczepański
- Sebastian Browarczyk
- Karol Niemykin
- Kacper Kaczor




